/*�������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define M 80

int getWord(char **words, char *str) //����������� ������� � ���������� �������� ������� ����� � ������
{
	int inWord=0;int i = 0, j = 0; 
	while (str[i] != '\n')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			words[j] = &str[i];
			j++;
		}
		else if (str[i] == ' ' && inWord == 1)
		{
			inWord = 0;
			words[j] = str + i - 1;
			j++;
		}
		i++;
	}
	words[j] = str + i-1;	 
	return j;
}
void MixWords(char **words, int j) // ������������� ��������� ������� �������� ������� ����� ����� 1 � ����������
{
	int k = 0, w=0;
	char temp;
	
	while (w<j)
	{
		if ((words[w+1] - words[w]) > 2)
		{
			words[w]++;
			words[w + 1]--;
			while ((words[w+1] - words[w]) > 0)
			{
				k = rand() % (words[w+1] - words[w] + 1);
				temp = *words[w+1];
				*words[w+1] = *(words[w] + k);
				*(words[w] + k) = temp;
				words[w+1]--;
			}
			words[w]--;
		}
		w += 2;
	}
}

int main()
{
	char str[M][M];
	char *words[M];
	int i=0, string=0;
	srand (time(0));
	FILE *in, *out;
	in = fopen("input.txt", "r");
	out = fopen("output.txt", "w");
	if (in == 0 || out == 0)
	{
		puts("File error");
		return 1;
	}
	while (fgets(str[i], M, in)) //��� ������������ �������� �����
		words[i] = str[i++];
	
	string = i; // ���-�� �����
	i = 0; // ��������� �����

	while (i < string)
	{
		MixWords(words, getWord(words, str[i]));
		fprintf(out, "%s", str[i]);
		i++;
	}
	fclose(in);
	fclose(out);
	return 0;
}