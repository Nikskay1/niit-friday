/*�������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� �������
���������:
������� ��������� ����� �������, ��� ������� ���� ����� ��������������
�������, � ����� �� ���� ��������.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int i = 0; //����� ������� � ������
	int j = 0; //����� ������� � �������
	int tempSim = 0;
	int tempNum = 0;
	int flag = 0;
	char str[80];
	char arr[80][2] = { 0 };
	int end = 0; //��������� ������ ������� �������������
	
	printf("Enter the string (1-80):\n");
	fgets(str, 80, stdin);
	
	while (str[i] != '\n')
	{
		j = 0;
		while (j <= end && flag == 0) //����� �� ������������� ���������
		{
			if (str[i] != arr[j][0])
				j++;
			else if (str[i] == arr[j][0])
			{
				flag = 1;
			}

		}

		if (flag == 0)                //������ ������ ������� � ������ �������������
		{
			arr[end][0] = str[i];
			end++;
		}
		i++;
		flag = 0;

	}
	i = 0; j = 0;
	while (j <= end)                 //������� ���������� ���������� ���������
	{
		arr[j][1] = 0;
		while (str[i] != '\n')
		{
			if (arr[j][0] == str[i])
				arr[j][1]++;
			i++;
		}
		i = 0;
		j++;
	}
	
    j = 1;
	
	while (j <= end) //������ ����������
	{
		if (j==0 || arr[j-1][1]>=arr[j][1]) //��� ���������� �� �������� ���� <=
			j++;
		else 
		{
			tempNum = arr[j][1];
			arr[j][1] = arr[j-1][1];
			arr[j-1][1]= tempNum;
			tempSim = arr[j][0];
			arr[j][0] = arr[j-1][0];
			arr[j-1][0]= tempSim;
			j--;
		}

	}
	
	printf("The array of frequency of occurrence of the symbols in descending order:\n");    //����� ������� �������������
	for (j = 0; j < end; j++)
	{
		printf(" %c %i\n ", arr[j][0], arr[j][1]);
	}
	printf("\n");

	return 0;
}