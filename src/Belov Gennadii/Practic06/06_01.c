// ��������� ��������� � ��������� ������� ����������� �����������
#include <stdio.h>
#include <math.h>
#define M   81      // 3 � ������� - ������ ������������ �����������
void printFr(char *arr, char *stop);
void fractal(int x, int y, int n, char(*arr)[M]);

int main()
{
	char arr[M][M] = { 0 };
	int i, j;
	fractal(M / 2, M / 2, pow(M, ((float)1 / 3)), arr);
		printFr(arr, arr+M);
	return 0;
}
void fractal(int x, int y, int n, char(*arr)[M])         // ������������ ��. ���
{
	if (n == 0)
	{
		arr[x][y] = '*';          // ������� ���������� - ������

	}
	else                         // ���
	{
		fractal(x, y, n - 1, arr);
		fractal(x + pow(3, n) / 3, y, n - 1, arr);
		fractal(x - pow(3, n - 1), y, n - 1, arr);
		fractal(x, y + pow(3, n) / 3, n - 1, arr);
		fractal(x, y - pow(3, n - 1), n - 1, arr);
	}
}
void printFr(char *arr, char *stop)         // ����� �� �����
{
	int i;
	for (i = (M - 1); arr<stop; i--, arr++)
	{
		if (*arr)
			putchar(*arr);
		else putchar(' ');

		if (!i)
		{
			putchar('\n');
			i = M;
		}
	}
}