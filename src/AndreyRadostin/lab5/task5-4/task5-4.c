//This program reads lines from a file and puts lines
// in function Print_words to change words' order and
// to write in other file

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define M 80


main()
{
		char str[M];
		FILE *in, *out;
		
		in = fopen("input5-4.txt", "r");
		out = fopen("output5-4.txt", "w");
		if (in == 0 || out == 0)
		{
			puts("File error!");
			return 1;
		}

		while (fgets(str, 80, in) != NULL)
		{
			Print_words(str, out);
			fprintf(out,"\n");
		}
		fclose(in);
		fclose(out);
		return 0;
}