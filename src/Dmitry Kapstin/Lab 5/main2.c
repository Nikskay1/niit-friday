/* �������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#define M 15

void clearArr(char);
void fillArr(char);

void copyArr(char str[M][M])
{
	for (int i = 0; i<M / 2; i++)
	for (int j = 0; j < M / 2; j++)
	{
		str[M - i - 1][j] = str[i][j];
		str[i][M - j - 1] = str[i][j];
		str[M - i - 1][M - j - 1] = str[i][j];
	}
}
void outArr(char str[M][M])
{
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < M; j++)
		{
			putchar(str[i][j]);
		}
		putchar('\n');
	}
	printf("________________________________________________________________\n");
}
	
int main()
{
	char str[M][M];
	
	
	while (1)
	{
		clearArr(str);
		fillArr(str);
		copyArr(str);
		outArr(str);
		Sleep(1000);
	}

	return 0;
}

void clearArr(char str[M][M])
{
	for (int i = 0; i<M; i++)
	  for (int j = 0; j < M; j++)
	  {
		 str[i][j] = ' ';
	  }
}

void fillArr(char str[M][M])
{
	srand(time(0));
	for (int i = 0; i<M/2; i++)
	  for (int j = 0; j < M/2; j++)
	  {
		if (rand()%2==0)
			str[i][j] = '*';
	  }
}
