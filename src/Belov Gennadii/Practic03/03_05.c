// ��������� ������� ����� ��������� ����� ������ ������������� � ��������� ������������� �������� �������
#include <stdio.h>
#include <time.h>
#define N 5 // ����� �������

int main(void)
{
	int arr[N];
	int i;
	int f_Minus, f_Plus;  // ����� ������� �������������� � �������������� ���������
	int numberMinus, numberPlus;
	int sumInterim, sum;

	srand(time(NULL));
	do
	{ 
		putchar('\r');
		numberMinus = numberPlus = -1;
		sumInterim = sum = 0;
		f_Minus = f_Plus = 1;
		

		for (i = 0; i < N; i++)
		{
			arr[i] = rand() % 20 - 10;  // �������� 
			printf("%d ", arr[i]);

			if (arr[i] < 0)
			{
				if (f_Minus)
				{
					numberMinus = i; f_Minus = 0;  // ������ ������������� �����
					sumInterim = 0; sum = 0;
				}
				else 
					sumInterim += arr[i];   // ������������� �����
			}
			
			if (arr[i] > 0)
			{
				if (f_Plus)
				{
					sum = sumInterim;
					sumInterim = arr[i];
					f_Plus = 0; numberPlus = i;  // ������ ������������� �����
				}
				else 
				{
					numberPlus = i;             // ��������� ������������, ���� ������ �� �������� ���������
				    sum += sumInterim;
				    sumInterim = arr[i];
				}
			}
			
		}
	} while (numberMinus < 0 || numberPlus < 0);  /* ��������� ��������� ������� ��� ���������� ������������� 
												      ��� ������������� �����*/

	printf("\n%d\n%d", arr[numberMinus], arr[numberPlus]);
	printf("\nsum = %d\n", sum);


	return 0;
}