/*�������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int i = 0;
	int j = 0;
	int count = 0; //��-�� ���� � ������
	int inWord = 0; //���� ����� �����
	int worlds = 0; //����� ����� ������� ����� �������
	char str[81];

	printf("Enter the string (1-80):\n");
	fgets(str, 80, stdin);
	printf("Enter the number of words:");
	scanf("%d", &worlds);
	while (str[i] != '\n') // ������� ���������� ����
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	while (worlds > count) //�������� ������������ ����� n 
	{
		printf("Error! Try entering words again - ");
		scanf("%d", &worlds);
	}
	i = 0;
	inWord = 0;
	count = 0;

	while (str[i] != '\n' && count != worlds) //����� n-�� ����� 
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
	    	i++;
    }
	j = i - 1;
	i = 0;
	while (str[i] != '\n'&& i!=j) //����� ������ ������ 
	{
		putchar(str[i]);
		i++;
	}
	while (str[i] != ' ' && str[i] != '\n') //������� n-�� ����� 
	{
		i++;
	}
	while (str[i] != '\n') //����� ������� ������
	{
		putchar(str[i]);
		i++;
	}
	printf("\n ");
	return 0;
}