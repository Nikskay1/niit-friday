// ��������� ������������ ��������� ������� ������� ������� ����� ������ ������ ����� ������� � ����������
//���������� ���������� ����� � ����
// ������ ������� �� ����� �����
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <locale.h>
#define N 80
void getWords(char *pStr, FILE * out);
void sortW(char *pStWord, char *pEnWord, FILE * out);
void printW(char *pStWord, char *pEnWord, FILE * out);

int main(void)
{
	char str[N];
	int i;
	setlocale(LC_CTYPE, "RUS");
	srand(time(NULL) | clock());

	FILE *in, *out;
	if ((in = fopen("rF.txt", "r")) == NULL)      // ���� ��� ������
	{
		puts("error opening file\n");
		exit(0);
	}

	if ((out = fopen("wF.txt", "w")) == NULL)  // ���� ��� ������
	{
		puts("error creating file\n");
		exit(0);
	}

	while (fgets(str, N, in))
		getWords(str, out);
	
	
	if (fclose(in) != 0 || fclose(out) != 0)
		puts("error closing files.\n");

	return 0;
}

void getWords(char *pStr, FILE * out)            // ���������� ����
{
	int i;
	int fWord = 0;
	char *pStWord, *pEnWord;

	pStWord = pEnWord = 0;
	for (i = 0; i<=strlen(pStr); i++)
	{
		if (pStr[i] == ' ' || pStr[i] == '\n' || pStr[i] == '\0')
		{
			if (pStWord && pEnWord)
			{
				if ((pEnWord - pStWord) < 3)        // ����� ������ ��� �������� � �����
					printW(pStWord, pEnWord, out);
				else
					sortW(pStWord, pEnWord, out);
				pStWord = pEnWord = 0;
			}
			fWord = 0;
			if (!pStr[i])
				break;
			fputc(pStr[i], out);    // ������ ��������, ����������� ��� ����
		}
		else
		{
			if (!fWord)
			{
				pStWord = pStr + i;
				fWord = 1;
			}
			pEnWord = pStr + i;
		}
	}
}

void sortW(char *pStWord, char *pEnWord, FILE * out)    // ����������
{
	int i, j;
	char pSymbol;
	
		for (i = 1; i < pEnWord - pStWord; i++)
		{
			do
				j = rand() % (pEnWord - pStWord - 1) + 1;  // ������� ������� �� �����������
			while (j == i);
			pSymbol = *(pStWord+i);
			*(pStWord + i) = *(pStWord + j);
			*(pStWord + j) = pSymbol;
			if ((pEnWord - pStWord - 1) == 2)  // ���� ��� ������� � ����������
				break;
		}
	
	printW(pStWord, pEnWord, out);
}

void printW(char *pStWord, char *pEnWord, FILE * out)    // ����� ����
{
	
	for (; pStWord<=pEnWord; pStWord++)
		fputc(*pStWord, out);
}