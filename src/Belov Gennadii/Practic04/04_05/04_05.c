// ��������� �������������� ������ � ������� �� ����������� �� ������ ����� � ������
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#define N 25 // ���-�� �����

int main()
{
	char str[N][75];
	char *strP[N];
	char *strIn; // ��������� ��� ����������
	int i=0, j, ii;
	FILE *in, *out;
	if ((in = fopen("rF.txt", "r")) == NULL)      // ���� ��� ������
	{
		puts("error opening file\n");   
		exit(0);
	}

	if ((out = fopen("wF.txt", "w")) == NULL)  // ���� ��� ������
	{
		puts("error creating file\n");    
		exit(0);
	}

	while (fgets(str[i], 75, in))      // ������ �����
	{
		if (str[i][0] == '\n')        // �� ������ ������
			break;
		strP[i] = str[i++];  // �������� ������
	}

	for (ii = 0; ii < i - 1; ii++)    // ���������� ����������
	{
		for (j = 0; j < i - ii - 1; j++)
		{
			if (strlen(strP[j]) > strlen(strP[j + 1]))
			{
				strIn = strP[j];
				strP[j] = strP[j + 1];
				strP[j + 1] = strIn;
			}
		}
	}

	for (j = 0; j<i; j++)          // ������ ����� �� ������� ����������
		fputs(strP[j], out);

	if (fclose(in) != 0 || fclose(out) != 0)
		puts("error closing files.\n");

	return 0;
}
