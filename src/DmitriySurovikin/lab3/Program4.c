/*�������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#define n 4

int main()
{
    int i = 0;
    int temp = 0;
    int sum = 0;
    int num = 0;
    int len = 0; //����� ������
    int razr = 0;//������ �����
    char str[81];
    printf("Enter the string (1-80):\n ");
    fgets(str, 80, stdin);
    len = strlen(str);


    while (i != len)
    {
        if (str[i] >= '0' && str[i] <= '9' && razr < n) //������� ������� �����
        {
            razr++;
            i++;
        }
        else if (razr > 0)                             //��������� ����� � ���������� ��� � �����
        {
            i -= razr;
            while (razr != 0)
            {
                razr--;
                temp = str[i] - 48;
                num = temp * pow(10, razr); //10 � ������� �������
                sum += num;
                i++;
            }
            razr = 0;
        }
        else i++;
    }



    printf("The sum of the numbers in the string - %d\n", sum);
    return 0;
}