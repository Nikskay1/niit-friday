//summ of elements between the minimal one and the maximal one
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20



int main()
{
	int i = 0, index_min = 0, index_max = 0,max=-100,min=100,temp;

	int summ = 0;
	int arr[N];
	srand(time(0));
	for (i = 0; i < N; ++i)
	{
		arr[i] = rand() % 19 - 9;
		if (arr[i]>max)
		{
			max = arr[i];
			index_max = i;
		}
		if (arr[i]<min)
		{
			min = arr[i];
			index_min = i;
		}

		printf("%d ", arr[i]);
	}
	printf("\n");
	printf("max= %d \n", arr[index_max]);
	printf("min= %d \n", arr[index_min]);
	if (index_min>index_max)
	{
		temp = index_max;
		index_max = index_min;
		index_min = temp;
	}
	
	for (i = index_min + 1; i < index_max; ++i)
		summ += arr[i];
	printf("Summ= %d \n", summ);


	return 0;
}