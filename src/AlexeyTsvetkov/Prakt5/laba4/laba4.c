#include <stdio.h>
#include <string.h>
#include <time.h>

#define N 80

void getWords(char *pStr, FILE * out);
void sortW(char **word, int *size, FILE * out);
void printW(char **word, int *size, FILE * out);

int main(void)
{
    char str[N];
    int i;
    srand(time(NULL) | clock());
    FILE *in, *out;
    if ((in = fopen("read.txt", "r")) == NULL)
    {
        puts("�訡�� ������!");
        exit(0);
    }
    if ((out = fopen("write.txt", "w")) == NULL)
    {
        puts("�訡�� �����!");
        exit(0);
    }
    while (fgets(str, N, in))
    {
        i = strlen(str) - 1;
        if (str[i] == '\n')
            str[i] = '\0';
        getWords(str, out);
    }
    if (fclose(in) != 0 || fclose(out) != 0)
        puts("�訡�� �������!");
    return 0;
}
void getWords(char *pStr, FILE * out)
{
    int i, j, g;
    int fWord = 0, fDelimiter = 0;
    char *pWord[N];
    for (i = 0, j = 0; pStr[i]; i++)
    {
        if (pStr[i] == ' ')
        {
            g = i; fWord = 0; fDelimiter = 1;
        }
        if (pStr[i] != ' ' && !fWord)
        {
            pWord[j++] = pStr + i;
            fWord = 1;
            if (fDelimiter)
                pStr[g] = '\0';
        }
    }
    sortW(pWord, &j, out);
}
void sortW(char **word, int *size, FILE * out)
{
    int i, j;
    char *pIn;
    if (*size > 1)
    {
        for (i = 0; i < *size; i++)
        {
            do
                j = rand() % *size;
            while (j == i);
            pIn = word[i];
            word[i] = word[j];
            word[j] = pIn;
            if (*size == 2)
                break;
        }
    }
    printW(word, size, out);
}
void printW(char **word, int *size, FILE * out)
{
    int i;
    for (i = 0; i<*size; i++)
        fprintf(out, "%s ", word[i]);
    fprintf(out, "\n");
}
