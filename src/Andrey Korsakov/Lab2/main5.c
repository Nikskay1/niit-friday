/*
�������� ���������, ������� ������� �� ����� 10 �������, ���������������
��������� ������� �� ��������� ���� � ����, ������ ����� ������ ����
��� � ������, ��� � � ������� ���������. ����� ������ - 8 ��������.
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int ran = 0;
	int i = 0;      //���-�� �����
	int j = 0;		//���-�� ��������
	
	srand(time(0));
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 8; j++)
		{
			ran = rand() % 3;
			if (ran == 0)
			{
				ran = 48 + rand() % 10;
			}
			else if (ran == 1)
			{
				ran = 65 + rand() % 26;
			}
			else
			{
				ran = 97 + rand() % 26;
			}
			printf("%c", ran);
		}
		printf("\n");
	}

	return 0;
}
