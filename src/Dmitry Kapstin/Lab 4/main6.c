/*�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 20
#define M 15

int main()
{
	int i = 0;
	int n = 0; //���-�� ������ �����
	int min=1000 , max=0 ; //���������� ���
	int year = 0; // �������
	char fam[N][M];
	char *old = fam[0], *young = fam[0];

	puts("How many relatives in your family?");
	scanf("%d", &n);

	for (; i < n; i++)
	{
		printf("Relative number %d :", i+1 ); //
		fflush(stdin);
		fgets(fam[i], N, stdin);
		fam[i][strlen(fam[i]) - 1] = 0;
		printf("How old is %s? ", fam[i]);
		scanf("%d", &year);
		
		if (min>year)
		{
			min = year;
			young = fam[i];
		}
		else if (max < year)
		{
			max = year;
			old = fam[i];
		}
	}
	printf("old - %s, young - %s\n", old, young);
	return 0;
}