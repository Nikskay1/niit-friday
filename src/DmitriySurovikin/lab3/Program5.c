/*�������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...)
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#define N 5

int main()
{
    int i = 0;
    int Otr = 0;
    int Pol = 0;
    int sum = 0;
    int otr = 0; //���� ��������������  �����
    int pol = 0; //���� ��������������  �����
    int str[N];
    srand(time(0));

    for (i = 0; i < N; i++)
    {
        str[i] = -100 + rand() % 200;
        printf("%d ", str[i]);
    }

    i = 0;
    while (i != N && otr == 0)
    {
        if (str[i] < 0) //����� ������� �������������
        {
            Otr = i;
            otr = 1;
            i++;
        }
        else i++;
    }
    i = N;
    while (i != 0 && pol == 0)
    {
        if (str[i] > 0) //����� ���������� ��������������
        {
            Pol = i;
            pol = 1;
            i--;
        }
        else i--;
    }
    for (i = Otr + 1; i != Pol; i++)
        sum += str[i];
    printf("\nThe sum of the numbers in the string: %d\n", sum);
    return 0;
}
