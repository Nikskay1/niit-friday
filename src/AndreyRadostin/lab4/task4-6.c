//This program request a number of relatives, their names and ages
//and demonsrates the names of the youngest and the oldest ones

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80


void printWord(char *p);


int main()
{
	int i = 0,num_rel,age_young=150,age_old=0,age_rel;
	char str[N][N], *young,*old;
	printf("Please, enter the number of Your relatives \n");
	scanf("%d", &num_rel);
	old = str[0];
	young = str[0];
	while (i<num_rel)
	{
		printf("Please, enter name of relative %d \n",i+1);
		scanf("%s",str[i]);
		printf("Please, enter age of %s \n", str[i]);
		scanf("%d", &age_rel);
		if (age_rel>age_old)
		{
			age_old = age_rel;
			old = str[i];
		}
		if (age_rel<age_young)
		{
			age_young = age_rel;
			young = str[i];
		}
		i++;
	}
	printf("The oldest relative is ");
	printWord(old);
	printf("\n");
	printf("The youngest relative is ");
	printWord(young);
	printf("\n");
	
	
	return 0;
}

void printWord(char *p)
{
	while (*p != '\0')
	{
		putchar(*p++);
	}
	putchar(' ');
}

