// ��������� ������� ����� � ��������� �� 2 �� 1000000, ����������� ����� ������� ������������������ ��������
#include <stdio.h>
#include <locale.h>
typedef unsigned long long ULL;
int fuSearch(ULL number);
int main()
{
	setlocale(LC_CTYPE, "RUS");
	int number, length;
	int nMax, lghtMax = 1;
	for (number = 2; number <= 1000000; number++)
	{
		if ((length = fuSearch(number))>lghtMax)         // ������� ������������ ����� ����-��
		{
			lghtMax = length;
			nMax = number;
		}
	}

	printf("����� %d ��������� ����� ������� ������������������ �� %d �����\n", nMax, lghtMax);
	return 0;
}
int fuSearch(ULL number)       // ��� ������� ����� ������� ����� ����-�� ������������������
{
	if (number == 1)
		return 1;
	if (number % 2)
		return fuSearch(3 * number + 1) + 1;
	else
		return fuSearch(number / 2) + 1;
}