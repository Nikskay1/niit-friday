/*
�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
*/

#include <stdio.h>
#include <string.h>

#define N 20
#define M 80

char str[N][M];
char *old, *young;
int i = 0, n = 0, year = 0, min = 100, max = 0;

int main()
{
	printf("How many relatives in your family? \n");					//������� ������������� � ����� �����?
	scanf("%d", &n);
	
	while (n > 0)
	{
		printf("What is the name of your %d relative? \n", i + 1);		//��� ����� ������ i ������������?
		fflush(stdin);
		fgets(str[i], 80, stdin);
		str[i][strlen(str[i])-1] = 0;
		printf("How many %d year relative? \n", i + 1);					//������� i ������������ ���?
		scanf("%d", &year);
		if (min > year)
		{
			min = year;
			young = str[i];
		}
		if (max < year)
		{
			max = year;
			old = str[i];
		}
		n--;
		i++;
	}
	printf("Your oldest relative: %s - %d year \n", old, max);				//���� ����� ������� �����������
	printf("Your youngest relative: %s - %d year\n", young, min);			//���� ����� ������� �����������

	return 0;
}