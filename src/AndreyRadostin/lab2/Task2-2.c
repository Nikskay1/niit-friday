//Program "Guess a number"
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<time.h>



int get_int();// function controling the integer input
int main()
{
	int x=0, y = 0, count=0;
	srand(time(0));
	x = rand() % 100+1;
	printf("Guess a number! \n");
	printf("Please, enter an integer number from 1 to 100 \n");
	do
	{
		y = get_int();
		count++;
		if (y<x) 
			printf("Your number is less than mine\n");
		if (y>x)
			printf("Your number is greater than mine\n");
		if (y==x)
			printf("You are lucky, You have done %d attemps\n",count);
	} while (y != x);
	
	printf("Thank You!\n");
	return 0;
}
int get_int()
{
	int input;
	char ch;
	while (scanf("%d", &input) != 1)
	{
		while ((ch = getchar()) != '\n')
			putchar(ch); // exemption from incorrect character
		printf(" is not integer. \n Please, input ");
		printf(" integer number: ");
	}
	return input;
}