/*�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 10
#define M 80

int main()
{
	int i = 0;
	int j = 0;
	int count = 0; //��-�� ����
	char temp = 0;
	char str[N][M];
	char *p[N];
    FILE *in, *out;
    

    in = fopen ("input.txt","r");

	out = fopen ("output.txt","w");
    if (in == 0 || out == 0)
    {puts ("File error");
    return 1;}

    /*while ((ch=fgetc(in))!=EOF//��� ������������� �������� �����, EOF ����� -1 (2 �����)
    {
    }*/

    while(fgets(str[i],M,in)) //��� ������������ �������� �����
		p[i] = str[i++];
	
	count = i;
	i = 1;
	while (i < count)     //������ ����������
	{
		if (i == 0 || strlen(p[i - 1]) <= strlen(p[i])) //��� ���������� �� ����������� ���� >=
			i++;
		else
		{
			char *temp = p[i];
			p[i] = p[i - 1];
			p[i - 1] = temp;
			i--;
		}
	}

	i = 0;
	while (i<count)
	{
		fprintf (out,"%s", p[i]);
		i++;
	}
    fclose(in);
    fclose(out);

    return 0;
}

