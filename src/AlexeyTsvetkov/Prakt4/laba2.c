#include <stdio.h>
#include <string.h>

#define N 80

int main()
{
    char str[N];
    char *pWord[N];
    char *pIn;
    int i, j, g;
    int fWord = 0, fDelimiter = 0;
    fgets(str, N, stdin);

    i = strlen(str) - 1;
    str[i] = '\0';

    for (i = 0, j = 0; str[i]; i++)
    {
        if (str[i] == ' ')
        {
            g = i; fWord = 0; fDelimiter = 1;
        }
        if (str[i] != ' ' && !fWord)
        {
            pWord[j++] = str + i;
            fWord = 1;
            if (fDelimiter)
                str[g] = '\0';
        }
    }

    for (i = 0, g = (j - 1); i<g; i++, g--)
    {
        pIn = pWord[i];
        pWord[i] = pWord[g];
        pWord[g] = pIn;
    }

    for (i = 0; i<j; i++)
        printf("%s ", pWord[i]);
    printf("\b\n");

    return 0;
}
