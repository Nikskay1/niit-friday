#include <stdio.h>
#include <string.h>
#include <time.h>

#define N 80

void getWords(char *pStr);
void sortW(char **word, int *size);
void printW(char **word, int *size);

int main(void)
{
    char str[N];
    int i;
    srand(time(NULL) | clock());
    fgets(str, N, stdin);
    i = strlen(str) - 1;
    str[i] = '\0';
    getWords(str);
    return 0;
}
void getWords(char *pStr)
{
    int i, j, g;
    int fWord = 0, fDelimiter = 0;
    char *pWord[N];
    for (i = 0, j = 0; pStr[i]; i++)
    {
        if (pStr[i] == ' ')
        {
            g = i; fWord = 0; fDelimiter = 1;
        }
        if (pStr[i] != ' ' && !fWord)
        {
            pWord[j++] = pStr + i;
            fWord = 1;
            if (fDelimiter)
                pStr[g] = '\0';
        }
    }
    sortW(pWord, &j);
}
void sortW(char **word, int *size)
{
    int i, j;
    char *pIn;
    if (*size > 1)
    {
        for (i = 0; i < *size; i++)
        {
            do
                j = rand() % *size;
            while (j == i);
            pIn = word[i];
            word[i] = word[j];
            word[j] = pIn;
            if (*size == 2)
                break;
        }
    }
    printW(word, size);
}
void printW(char **word, int *size)
{
    int i;
    for (i = 0; i<*size; i++)
        printf("%s ", word[i]);
    printf("\b\n");
}
