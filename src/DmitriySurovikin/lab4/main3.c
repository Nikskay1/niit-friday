// �������� ���������, ������� ����������� ������ � ����������, �� �������� �� ������ ����������� (��������� �������� � ����� ������� � ������ ������) 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 80

int Palindr(char *str);

int main()
{
    int i = 0, j;
    char str[N];
    printf("Vvedite stroku \n");
    fgets(str, 80, stdin);

    i = strlen(str) - 1;
    str[i] = '\0 ';

    j = Palindr(str);
    if (j == 1)
        printf("Yes \n");
    else
        printf("No \n");

    return 0;
}
int Palindr(char *str)
{
    char *start = str, *end = str + strlen(str) - 2;
    while (start <= end)
    {
        if (*start != *end)
            return 0;
        start++; end--;
    }
    return 1;
}