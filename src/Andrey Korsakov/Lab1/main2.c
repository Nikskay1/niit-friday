/*
�������� ���������, ������� ����������� ������� ����� � ���-
���� ��:��:��, � ����� ������� ����������� � ����������� ��
���������� ������� ("������ ���� "������ ����"� �.�.)
*/
#include <stdio.h>

int main()
{
	int hour = 0;
	int minutes = 0;
	int seconds = 0;

	printf("Enter the time (hh:mm:ss)\n");
	scanf("%d:%d:%d", &hour, &minutes, &seconds);
	if (hour >= 0 && hour <= 24 && \
		minutes >= 0 && minutes <= 59 && \
		seconds >= 0 && seconds <= 59)
	{
		if (hour >= 5 && hour <= 10)		//����
			printf("Good morning!\n");
		else if (hour > 10 && hour <= 18)	//����
			printf("Good afternoon!\n");
		else if (hour > 18 && hour <= 22)	//�����
			printf("Good evening!\n");
		else                                //����
			printf("Good night!\n");
	}
	else
		printf("The time was entered incorrectly\n");

	return(0);
}