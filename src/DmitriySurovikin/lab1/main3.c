#include <stdio.h>

int main()
{
	float chislo = 0;
	char a=0;
	printf("Vvedite zna4enie ugla v gradusax (45.00D) ili v radianax (45.00R): ");
	scanf("%f%c", &chislo, &a);
	float chislo2 = chislo * 3.14 / 180;
	float chislo3 = chislo * 180 / 3.14;
	if (a == 'D')
		printf("Zna4enie ugla v radianax = %fR", chislo2);
	else if (a == 'R')
		printf("Zna4enie ugla v gradusax = %fD", chislo3);
	else 
		printf("Error!");
	puts("");
	return 0;

}