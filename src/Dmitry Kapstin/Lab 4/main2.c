/*�������� ���������, ������� � ������� ������� ���������� ������� �����
������ � �������� �������
���������:
������ �� ������� ��������� ������ ���������� �� char, � ������� ���������
������ ������ �������� ������� ����� (������������ - ������ ������� � ��-
�������� ��������). ����� �� ���������� ����� ����� ������, ��������� ����
������ �� ����������.
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 80
#define M 80

int main()
{
	int i = 0;
	int j = 0;
	int inWord = 0;
	int k=0; 
	
	char str[M];
	char out[M]; //������ ������ ������ ��������
	char *p[M];

	printf("Enter the string (1-80):\n");
	fgets(str, N, stdin);
	
	while (str[i] != '\n')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			p[j] = &str[i];
			j++;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	p[j] = &str[i];                  //���������� ������ ����� ������
	
	for (j; j > 0; j--)
	{

		i = p[j] - p[j-1];		     //��������� ���������� ���� � �����
		
		memcpy(out, p[j - 1], i);	//�������� ����� ����� ��������� � ����� ������
		k = 0;
		while (k < i)
		{
			printf("%c", out[k]);
			k++;
		}
	}
	
	return 0;
}